package com.radiologics.security.oauth.resourceServer.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.util.AntPathMatcher;

import javax.servlet.http.HttpServletRequest;

@AllArgsConstructor
public class XnatClientCredentialsRequestMatcher implements RequestMatcher {

    @Getter
    final String path;

    @Getter
    final String scope;

    @Override
    public boolean matches(HttpServletRequest request) {
        final String auth = request.getHeader("Authorization");
        final AntPathMatcher pathMatcher = new AntPathMatcher();
        return (auth != null && auth.startsWith("Bearer") && pathMatcher.match(path, request.getRequestURI()));
    }
}
