package com.radiologics.security.oauth.resourceServer.provider;

import lombok.extern.slf4j.Slf4j;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.security.helpers.Users;
import org.nrg.xft.security.UserI;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationManager;

import java.util.ArrayList;
import java.util.Arrays;

@Slf4j
public class XnatOAuth2AuthenticationManager extends OAuth2AuthenticationManager {

    public static final SimpleGrantedAuthority AUTHORITY_OAUTH_ANON   = new SimpleGrantedAuthority("ROLE_OAUTH_ANON");

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        authentication = super.authenticate(authentication);
        if (authentication != null && authentication.isAuthenticated()) {
            // XNAT needs a UserI object in the authentication principal.  See: XDAT.getUserDetails()
            return new UsernamePasswordAuthenticationToken(buildOauthUser(), "",
                    new ArrayList<GrantedAuthority>(Arrays.asList(AUTHORITY_OAUTH_ANON,
                                                                  Users.AUTHORITY_USER,
                                                                  Users.AUTHORITY_DATA_ACCESS,
                                                                  Users.AUTHORITY_DATA_ADMIN)));
        }
        return authentication;
    }

    private UserI buildOauthUser(){
            UserI user = new XDATUser();
            user.setLogin("OAUTH_USER");
            user.setEmail("OAUTH_USER");
            user.setFirstname("OAUTH_USER");
            user.setLastname("OAUTH_USER");
            user.setPassword("");
            user.setEnabled(Boolean.TRUE);
            user.setVerified(Boolean.TRUE);
            return user;
    }
}