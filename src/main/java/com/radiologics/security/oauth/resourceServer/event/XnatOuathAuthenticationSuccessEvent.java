package com.radiologics.security.oauth.resourceServer.event;

import org.springframework.security.authentication.event.AbstractAuthenticationEvent;
import org.springframework.security.core.Authentication;

public class XnatOuathAuthenticationSuccessEvent extends AbstractAuthenticationEvent {
    public XnatOuathAuthenticationSuccessEvent(Authentication authentication) {
        super(authentication);
    }
}
