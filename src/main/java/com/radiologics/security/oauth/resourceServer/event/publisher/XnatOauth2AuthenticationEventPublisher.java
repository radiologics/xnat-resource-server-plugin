package com.radiologics.security.oauth.resourceServer.event.publisher;

import com.radiologics.security.oauth.resourceServer.event.XnatOauthAuthenticationFailureEvent;
import com.radiologics.security.oauth.resourceServer.event.XnatOuathAuthenticationSuccessEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
public class XnatOauth2AuthenticationEventPublisher implements AuthenticationEventPublisher {

    @Autowired
    private ApplicationEventPublisher _publisher;

    public XnatOauth2AuthenticationEventPublisher(ApplicationEventPublisher publisher){
        _publisher = publisher;
    }
    @Override
    public void publishAuthenticationSuccess(Authentication authentication) {
        _publisher.publishEvent(new XnatOuathAuthenticationSuccessEvent(authentication));
    }

    @Override
    public void publishAuthenticationFailure(AuthenticationException exception, Authentication authentication) {
        _publisher.publishEvent(new XnatOauthAuthenticationFailureEvent(authentication, exception));
    }
}
