package com.radiologics.security.oauth.resourceServer.event;

import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

public class XnatOauthAuthenticationFailureEvent extends AbstractAuthenticationFailureEvent {
    public XnatOauthAuthenticationFailureEvent(Authentication authentication, AuthenticationException exception) {
        super(authentication, exception);
    }
}
