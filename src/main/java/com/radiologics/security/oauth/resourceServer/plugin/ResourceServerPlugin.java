package com.radiologics.security.oauth.resourceServer.plugin;

import com.radiologics.security.oauth.resourceServer.provider.XnatOAuth2AuthenticationManager;
import com.radiologics.security.oauth.resourceServer.provider.token.CustomAccessTokenConverter;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.jwk.JwkTokenStore;

@ComponentScan({
        "com.radiologics.security.oauth.resourceServer.config",
        "com.radiologics.security.oauth.resourceServer.event",
        "com.radiologics.security.oauth.resourceServer.provider.token"
})
@XnatPlugin(value = "resource-server-plugin",
        name = "XNAT Resource Server",
        description = "Enables Resource Server for XNAT",
        logConfigurationFile = "META-INF/resources/resource-server-logback.xml")
public class ResourceServerPlugin {

    @Bean
    public TokenStore tokenStore(CustomAccessTokenConverter accessTokenConverter,
                                 @Value("${oauth2.keySetUri}") String keySetUri) {
        return new JwkTokenStore(keySetUri, accessTokenConverter);
    }

    @Bean
    public DefaultTokenServices tokenServices(TokenStore tokenStore) {
        final DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(tokenStore);
        return defaultTokenServices;
    }

    @Bean
    public XnatOAuth2AuthenticationManager xnatOAuth2AuthenticationManager(DefaultTokenServices tokenServices){
        XnatOAuth2AuthenticationManager xnatOAuth2AuthenticationManager = new XnatOAuth2AuthenticationManager();
        xnatOAuth2AuthenticationManager.setTokenServices(tokenServices);
        return xnatOAuth2AuthenticationManager;
    }

    @Bean
    public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        PropertySourcesPlaceholderConfigurer properties =
                new PropertySourcesPlaceholderConfigurer();
        properties.setLocation(new FileSystemResource("/home/xnat/config/xnat-oauth2.properties"));
        properties.setIgnoreResourceNotFound(false);
        return properties;
    }
}
