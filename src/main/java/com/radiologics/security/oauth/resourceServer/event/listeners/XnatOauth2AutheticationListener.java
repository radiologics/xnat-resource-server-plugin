package com.radiologics.security.oauth.resourceServer.event.listeners;

import com.radiologics.security.oauth.resourceServer.event.XnatOauthAuthenticationFailureEvent;
import com.radiologics.security.oauth.resourceServer.event.XnatOuathAuthenticationSuccessEvent;
import lombok.extern.slf4j.Slf4j;
import org.nrg.xdat.turbine.utils.AccessLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AbstractAuthenticationEvent;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;


@Component
@Slf4j
public class XnatOauth2AutheticationListener implements ApplicationListener<AbstractAuthenticationEvent>  {

    @Autowired
    HttpServletRequest request;

    @Override
    public void onApplicationEvent(AbstractAuthenticationEvent event) {
        if (event instanceof XnatOuathAuthenticationSuccessEvent) {
//            logAccess("SUCCESS");
        }else if(event instanceof XnatOauthAuthenticationFailureEvent) {
//            final String reason = ((XnatOauthAuthenticationFailureEvent) event).getException().getMessage();
//            logAccess(String.format("FAILED (Reason: %s)", reason));
        }
    }

    private void logAccess(String message){
        // Just hard code OAUTH_USER for now until we can get stuff from event.getAuthentication().getDetails()
//        AccessLogger.LogServiceAccess("OAUTH_USER", request, "Oauth2 Authentication", message);
    }
}
