package com.radiologics.security.oauth.resourceServer.config;

import com.radiologics.security.oauth.resourceServer.event.publisher.XnatOauth2AuthenticationEventPublisher;
import com.radiologics.security.oauth.resourceServer.provider.XnatOAuth2AuthenticationManager;
import com.radiologics.security.oauth.resourceServer.provider.token.CustomAccessTokenConverter;
import com.radiologics.security.oauth.resourceServer.util.XnatClientCredentialsRequestMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.FileSystemResource;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.jwk.JwkTokenStore;

import java.util.List;

@Configuration // includes component scanning
@EnableResourceServer // for enabling a Spring Security filter that authenticates requests via an incoming OAuth2 token
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Autowired
    XnatOAuth2AuthenticationManager xnatOAuth2AuthenticationManager;

    @Autowired
    XnatOauth2AuthenticationEventPublisher eventPublisher;

    @Autowired
    List<XnatClientCredentialsRequestMatcher> requestMatchers;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        XnatClientCredentialsRequestMatcher[] matchers = requestMatchers.toArray(new XnatClientCredentialsRequestMatcher[requestMatchers.size()]);
        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
            .and()
                .requestMatchers().requestMatchers(matchers);

        for(XnatClientCredentialsRequestMatcher matcher : matchers){
            http.authorizeRequests().requestMatchers(matcher).hasAuthority(matcher.getScope());
        }
    }

    @Override
    public void configure(final ResourceServerSecurityConfigurer config) {
        config.authenticationManager(xnatOAuth2AuthenticationManager)
                .eventPublisher(eventPublisher)
                .resourceId(null)
                .stateless(false);
    }
}
